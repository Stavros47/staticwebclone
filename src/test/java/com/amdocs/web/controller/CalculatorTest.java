package com.amdocs;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {

     @Test
     public void testAdd () throws Exception {
          int res = new Calculator().add();
          assertEquals("Test add", 9, res);
     }

    @Test
     public void testSub () throws Exception {
           int res = new Calculator().sub();
           assertEquals("Test sub", 3 , res);

     } 

}
